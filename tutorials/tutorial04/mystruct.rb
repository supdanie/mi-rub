#!/bin/ruby

class MyStruct
  def initialize(params={})
    params.each do |param, value|

      # There could be added a variable with name for example delete_field or inspect. For this reason I check whether the variable name is not equal to a name of any method of this class.
      define_variable(param, value)
    end
  end


  def delete_field(field)
     name_of_field = field.to_s
     name_of_setter = "#{name_of_field}="
     self.singleton_class.remove_method name_of_setter.to_sym
     # Removing getter, if it is defined and it is a getter, not another method such as to_h, inspect or delete_field.
     if (self.singleton_class.method_defined?(field.to_sym) && MyStruct.new.singleton_class.method_defined?(field.to_sym) == false)
       self.singleton_class.remove_method field.to_sym
     end
     remove_instance_variable("@#{field}")
  end


  def to_h
    declarations = Hash.new
    self.instance_variables.map do |attribute|
      name_of_variable = attribute.to_s[1...].to_sym
      value = self.instance_variable_get(attribute)
      declarations[name_of_variable] = value
    end
    declarations
  end

  def to_s
    string="#<MyStruct "
    declarations = []
    self.instance_variables.map do |attribute|
      value = self.instance_variable_get(attribute)
      declarations.push("#{attribute[1..]}=#{value}")
    end
    string+="#{declarations.join(', ')}>"
    string
  end

  def method_missing(name, *args)
    count = 0
    args.each do |arg|
      count += 1
    end
    if count == 0
      nil
    elsif name.to_s.end_with?("=") && count == 1
      name_of_variable = name.to_s[0..-2]
      value = args[0]
      
      # There could be added a variable with name for example delete_field or inspect. For this reason I check whether the variable name is not equal to a name of any method of this class.
      define_variable(name_of_variable, value)
    else
      raise ("No method found with name #{name}. You called #{name} with #{args.join(',')} and it failed.")
    end
  end

  def define_variable(name, value)
    if (self.singleton_class.method_defined?("#{name}".to_sym) == false)
      self.singleton_class.define_method "#{name}" do
        instance_variable_get("@#{name}")
      end
    end

    self.singleton_class.define_method "#{name}=" do |value|
      instance_variable_set("@#{name}", value)
    end

    instance_variable_set("@#{name}", value)
  end

end

cl = MyStruct.new("a" => 2, "b" => 3, "c" => "string", :dada => String.new.class)
p cl
p cl.methods
p cl.d
p cl.a
cl.a = 5
cl.delete_field = 5
p cl.a
cl.sto = "fff"
p cl
cl.delete_field("c")
p cl
p cl.to_h
p cl.methods
cl.delete_field(:delete_field)
p cl
cl.delete_field(:a)
p cl

cl2 = MyStruct.new(:d => "string2")
p cl2
p cl2.a
