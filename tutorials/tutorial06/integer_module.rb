# frozen_string_literal: true

# IntegerModule refines Integer to be convertable to a roman number.
module IntegerModule
  refine Integer do
    # This method compares the actual integer with a roman number
    # (object of the Roman class).
    #
    # @param [Roman] The roman number compared with the actual integer.
    # @return [Boolean] Whether the roman number is equal to the actual integer.
    def eql?(other)
      value = other.class == Roman ? other.value : other
      self == value
    end

    # Method to_rom converts the actual integer to a roman number.
    #
    # @return [Roman] The roman number created from the actual integer.
    def to_rom
      Roman.new(self)
    end
  end
end
