# Ovládání utility

Ke spuštění utility je potřeba nainstalovat thor příkazem `gem install thor`. CLI lze spustit pomocí thor romancli. Tato utilita obsahuje help pro nápovědu, to_roman pro převod arabského čísla na římské číslo a to_arabic pro převod římského čísla na arabské.

## Převod arabského čísla na římské

`thor romancli:to_roman NUM`, kde NUM je celé číslo od 1 do 3000, jinak se vypíše chyba, převede arabské číslo na římské číslo

Příklad volání

`thor romancli:to_roman 25` - převede číslo 25 na římské číslo a vypíše XXV

## Převod římského čísla na arabské

`thor romancli:to_arabic ROMAN`, kde ROMAN je nějaký řetězec reprezentující římské číslo od 1 do 3000, převede dané římské číslo na arabské číslo a vypíše arabské číslo, pokud nezadáme číslo, nebo číslo bude mimo interval od 1 do 3000, vypíše se chyba

Příklad volání

`thor romancli:to_arabic MDCCXVII` - převede římské číslo MDCCXVII na araské číslo a vypíše 1717

## Zobrazení nápovědy

`thor romancli:help COMMAND` - zobrazení nápovědy pro příkaz COMMAND, kde COMMAND může být to_roman, nebo to_arabic

Příklad volání

`thor romancli:help to_roman` - zobrazí nápovědu k příkazu to_roman
