# frozen_string_literal: true

require_relative './integer_module.rb'
# frozen_string_literal: true

require_relative './string_module.rb'
# frozen_string_literal: true

require_relative './invalid_data_type_error.rb'
# frozen_string_literal: true

require_relative './invalid_string_value_error.rb'
# frozen_string_literal: true

require_relative './invalid_integer_value_error.rb'

using IntegerModule

# Roman is a class represents a roman number.
class Roman
  include Comparable
  # Value is an attribue accessor for getting the integer value of
  # the roman number.
  attr_accessor :value
  # to_int is an alias for the value variable where is saved the
  # arabic integer value of this roman number.
  alias to_int value

  # This is the constructor of the roman number. There is checked
  # whether the number is in the interval from 1 to 3000
  # or the string represents a roman number from 1 to 3000.
  # If there is given a different number or a string
  # which does not represent a roman number from 1 to 3000 or another object,
  # there is thrown an error of the correct type
  # (InvalidDataTypeError if the given object is not Integer nor String),
  # InvalidIntegerValueError if there is given an integer
  # which is outside the interval from 1 to 3000 or InvalidStringValueError
  # if there is given a String which does not represent a roman number
  # from 1 to 3000).
  #
  # @param [Class] value Integer or string representing the roman number.
  def initialize(value)
    bad_class = 'Value must be an integer or a string.'
    bad_class_cond = value.class != Integer && value.class != String
    raise InvalidDataTypeError, bad_class if bad_class_cond

    if value.class == String
      match = /([M]{0,2}((CM)|(CD)|([D]?[C]{0,3}))?((XC)|(XL)|([L]?[X]{0,3}))?((IX)|(IV)|([V]?[I]{0,3}))?)/.match(value)
      initialize_with_string(value, match)
    else
      initialize_with_integer(value)
    end
  end

  # initialize_with_string initializes the roman number with the given string.
  # If there is given invalid string there is raised
  # the InvalidStringValueError exception.
  #
  # @param [String] value The string representing the roman number.
  # @param [String] match The string representing the correct match
  #   with the regular expression for a roman number.
  #
  def initialize_with_string(value, match)
    no_match = 'The value must be a valid Roman number.'
    out_range = 'The value must be a valid Roman number. The value must contain a Roman number from 1 to 3000, but the value must be a Roman number only.'
    no_match_cond = match.nil? || value == ''
    out_range_cond = match.to_s != value && value != 'MMM'

    raise InvalidStringValueError, no_match if no_match_cond

    raise InvalidStringValueError, out_range if out_range_cond

    self.value = string_to_value(value)
  end

  # initialize_with_integer initializes the roman number with the given integer.
  # If there is given an integer outside the interval from 1 to 3000
  # there is thrown a InvalidIntegerValueError exception.
  #
  # @param [Integer] value The integer representing the roman number.
  def initialize_with_integer(value)
    out_range = 'Value must be positive (not zero or negative).'
    out_range_cond = value <= 0 || value > 3000

    raise InvalidIntegerValueError, out_range if out_range_cond

    self.value = value
  end

  # This method compares the actual object to the second given object.
  #
  # @param [Class] other The roman number (or an integer) which is
  #   the actual object compared to.
  # @return [Boolean] Whether the object is equal, smaller or greater
  #   than the second given object
  def <=>(other)
    value <=> other
  end

  # This eql? method returns whether the roman number is equal to
  # the given second roman number (or an integer).
  #
  # @param [Class] other The roman number (or an integer) which is
  #   the actual object compared to.
  # @return [Boolean] Whether the given roman number
  #   is equal to the actual roman number.
  def eql?(other)
    other == value
  end

  # This + method sums two romans number or the actual roman number
  # with an integer.
  #
  # @param [Class] other The roman number or an Integer
  #   which is the second operand.
  # @return [Integer] The value which is the sum of the actual roman number
  #   with the given second roman number or the given integer.
  def +(other)
    value + other
  end

  # This - method subtracts the given roman number or the given integer
  # from the actual roman number.
  #
  # @param [Class] other The roman number or an Integer which is
  #   the second operand.
  # @return [Integer] The difference between the actual roman number
  #   and the given second operand.
  def -(other)
    value - other
  end

  # This * method multiplies the given roman number or the given integer with
  # the actual roman number.
  #
  # @param [Class] other The roman number or an Integer which is
  #   the second operand.
  # @return [Integer] The product of the actual roman number and
  #   the given second operand.
  def *(other)
    value * other
  end

  # This / method divides the actual roman number by the given roman number
  # or the given integer.
  #
  # @param [Class] other The roman number or an Integer which is
  #   the second operand.
  # @return [Integer] The quotient of the actual roman number and
  #   the given second operand.
  def /(other)
    value / other
  end

  # string_values_for_orders returns an array of array of
  # roman number for each digit of arabic number.
  #
  # @return [Array] The array of arrays with roman numbers.
  def string_values_for_orders
    units = %w[I II III IV V VI VII VIII IX]
    tens = %w[X XX XXX XL L LX LXX LXXX XC]
    hundreds = %w[C CC CCC CD D DC DCC DCCC CM]
    thousands = %w[M MM MMM MMMM MMMMM MMMMMM MMMMMMM MMMMMMMM MMMMMMMMMM]
    [units, tens, hundreds, thousands]
  end

  # This to_s method creates a string from the actual value
  # of this roman number and returns it.
  #
  # @return [String] The string representing the actual roman number got from
  #   the value.
  def to_s
    value = self.value
    value_to_string = ''
    [0, 1, 2, 3].each do |order|
      if value % 10 != 0
        string_for_order = string_values_for_orders[order][(value % 10) - 1]
        value_to_string = "#{string_for_order}#{value_to_string}"
      end
      value /= 10
    end
    value_to_string
  end

  # This method coerces the two operands to be in the correct order
  # for any operation.
  #
  # @param [Class] other The second operand
  #   (roman number (Roman class) or an Integer).
  # @return [Array] The array with two operands in the correct order for
  #   an operation (it depends on a type of the second operand).
  def coerce(other)
    if other.class != Roman
      [other, value]
    else
      [self, other]
    end
  end

  # string_to_value method converts the given string value to an integer
  # representing the arabic number which is equal to the roman number.
  #
  # @param [String] value The string with this roman number
  # @return [Integer] An integer representing the arabic number
  #   which is equal to the given roman number.
  def string_to_value(value)
    string_value = value.dup
    integer_value = 0
    order = 1
    (0..3).each do |index|
      highest = find_highest_for_order(index, string_value)
      string_value.gsub!(highest[1], '')
      integer_value += highest[0] * order
      order *= 10
    end
    integer_value
  end

  # find_highest_for_order finds the digit in the rome string.
  # It helps to convert the string with the roman number to an integer.
  #
  # @param [Integer] index The index of the order
  #   (0 for units, 1 for tens, 2 for hundreds).
  # @param [String] string_value The given string with roman number
  #   which can be processes (removed previous roman digits).
  # @return [Array] The array with an integer with the arabic digit
  #   and the string with roman digit included in the roman number.
  def find_highest_for_order(index, string_value)
    highest_found = 0
    highest_string_value = ''
    (1..9).each do |number|
      rome_string = string_values_for_orders[index][number - 1]
      next if string_value.include?(rome_string) == false

      highest_found = number
      highest_string_value = rome_string
      break if number == 4
    end
    [highest_found, highest_string_value]
  end
end
