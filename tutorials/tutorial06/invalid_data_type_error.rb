# frozen_string_literal: true

# InvalidDataTypeError is an error which is thrown when the initialize method in
# in the Roman class is called with an argument which is not an Integer
# or a String.
class InvalidDataTypeError < StandardError
  # This method returns a message describing the error when the initializer
  # of the Roman class is called with an argument which is not an Integer
  # or a String.
  #
  # @return [String] The string describing the error when the initializer
  #   of the Roman class is called with an argument which is not an Integer
  #   or a String and is printed in the console in this case.
  def message
    "Error: Invalid data type: > #{super}"
  end
end
