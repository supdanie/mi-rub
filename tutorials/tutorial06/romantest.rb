# frozen_string_literal: true

require 'minitest/autorun'
# frozen_string_literal: true

require_relative './roman.rb'

using IntegerModule
using StringModule

# RomanTest is a class which tests the methods of the Roman class.
class RomanTest < Minitest::Test
  # test_creating_correct_roman creates one roman number and tests
  # whether the roman number has correct value and whether it is
  # correctly converted to string (to_s method).
  def test_creating_correct_roman
    roman = Roman.new(56)
    assert_equal 56, roman.value
    assert_equal 'LVI', roman.to_s
  end

  # test_creating_correct_romans_with_operations_i tests creating
  # a few romans number and basic mathematical operation with them.
  # This method test +, -, * and / with the created roman numbers.
  def test_creating_correct_romans_with_operations_i
    roman1 = Roman.new(5)
    roman2 = Roman.new(6)
    roman3 = Roman.new(7)
    roman4 = Roman.new(8)
    roman5 = Roman.new(9)
    assert_equal 26, roman1 + roman2 + roman3 + roman4
    assert_equal 4, roman5 - roman1
    assert_equal 12, roman5 * roman4 / roman2
  end

  # test_creating_correct_romans_with_operations_ii tests creating
  # a few romans number and basic mathematical operation with them.
  # This method test +, -, * and / with the created roman numbers.
  def test_creating_correct_romans_with_operations_ii
    roman3 = Roman.new(7)
    roman4 = Roman.new(8)
    roman5 = Roman.new(9)
    assert_equal 24, 33 - roman5
    assert_equal 3, roman3 - 4
    assert_equal 3, 27 / roman5
    assert_equal 4, roman4 / 2
    assert_equal 8, roman4
  end

  # test_creating_correct_romans_with_operations_iii tests creating
  # two romans number and basic mathematical operation with them.
  # This method test +, -, * and / with the created roman numbers.
  # This method also test comparing the roman numbers and comparing
  # any of the two roman numbers with an integer.
  def test_creating_correct_romans_with_operations_iii
    roman4 = Roman.new(8)
    roman5 = Roman.new(9)
    assert_equal true, roman4 == 8
    assert_equal true, roman4.eql?(8)
    assert_equal true, 8.eql?(roman4)
    assert_equal 20.class, (roman5 * roman4).class
  end

  # test_operations_with_larger_romans test mathematical operations
  # with romans with larger value (from 1000 to 2000).
  def test_operations_with_larger_romans
    roman1 = Roman.new(1_500)
    roman2 = Roman.new(1_000)
    roman3 = Roman.new(2_000)
    assert_equal 1_500_000, roman1 * roman2
    assert_equal 4_500, roman1 + roman2 + roman3
    assert_equal 2_500, roman3 - roman2 + roman1
    assert_equal 1_500, roman3 + roman2 - roman1
  end

  # test_operations_with_larger_romans_ii test mathematical operations
  # with romans with larger value (from 1000 to 2000).
  def test_operation_with_larger_romans_ii
    roman1 = Roman.new(1_500)
    roman2 = Roman.new(1_000)
    roman3 = Roman.new(2_000)
    roman4 = Roman.new(3_000)
    assert_equal 3_000, roman3 / roman2 * roman1
    assert_equal 1_333, roman3 * roman2 / roman1
    assert_equal 7_500, (roman3 + roman4) / roman2 * roman1
  end

  # test_invalid_string_error_romans tests whether it is raised
  # InvalidStringValueError exception when the initialize method
  # of the Roman class is called with a string which does not represent
  # a roman number from 1 to 3000.
  def test_invalid_string_error_romans
    assert_raises(InvalidStringValueError) { Roman.new('MMXXL') }
    assert_raises(InvalidStringValueError) { Roman.new('MMCCD') }
    assert_raises(InvalidStringValueError) { Roman.new('MMMI') }
    assert_raises(InvalidStringValueError) { Roman.new('') }
    assert_raises(InvalidStringValueError) { Roman.new('non roman') }
  end

  # test_invalid_string_error_romans_second tests whether it is raised
  # InvalidStringValueError exception when the initialize method
  # of the Roman class is called with a string which does not represent
  # a roman number from 1 to 3000.
  def test_invalid_string_error_romans_second
    assert_raises(InvalidStringValueError) { Roman.new('IXX') }
    assert_raises(InvalidStringValueError) { Roman.new('IVI') }
    assert_raises(InvalidStringValueError) { Roman.new('XCC') }
    assert_raises(InvalidStringValueError) { Roman.new('XLX') }
    assert_raises(InvalidStringValueError) { Roman.new('CMM') }
    assert_raises(InvalidStringValueError) { Roman.new('CDC') }
  end

  # test_invalid_string_error_romans_invalid_format tests whether it is raised
  # InvalidStringValueError exception when the initialize method
  # of the Roman class is called with a string which does not represent
  # a roman number from 1 to 3000. This method tests creating roman numbers
  # in invalid format.
  def test_invalid_string_error_romans_invalid_format
    assert_raises(InvalidStringValueError) { Roman.new('AMMCDXLV') }
    assert_raises(InvalidStringValueError) { Roman.new('MMCDXLVA') }
    assert_raises(InvalidStringValueError) { Roman.new('I non roman') }
    assert_raises(InvalidStringValueError) { Roman.new('non roman VI') }
    assert_raises(InvalidStringValueError) { Roman.new('2II') }
    assert_raises(InvalidStringValueError) { Roman.new('II2') }
    assert_raises(InvalidStringValueError) { Roman.new('2') }
  end

  # test_invalid_string_error_romans_two_subtracts tests whether it is raised
  # InvalidStringValueError exception when the initialize method
  # of the Roman class is called with a string which does not represent
  # a roman number from 1 to 3000. It test calling the initializer
  # of the Roman class with strings like IIV or XXC.
  def test_invalid_string_error_romans_two_subtracts
    assert_raises(InvalidStringValueError) { Roman.new('IIV') }
    assert_raises(InvalidStringValueError) { Roman.new('IIX') }
    assert_raises(InvalidStringValueError) { Roman.new('XXL') }
    assert_raises(InvalidStringValueError) { Roman.new('XXC') }
    assert_raises(InvalidStringValueError) { Roman.new('CCD') }
    assert_raises(InvalidStringValueError) { Roman.new('CCM') }
  end

  # test_creating_negative_or_zero_romans test whether it is raised
  # InvalidIntegerValueError exception when the initializer of the
  # Roman class is called with a negative or zero integer.
  def test_creating_negative_or_zero_romans
    assert_raises(InvalidIntegerValueError) { Roman.new(0) }
    assert_raises(InvalidIntegerValueError) { Roman.new(-10) }
    assert_raises(InvalidIntegerValueError) { Roman.new(-145) }
    assert_raises(InvalidIntegerValueError) { Roman.new(-1) }
    assert_raises(InvalidIntegerValueError) { Roman.new(-2_543_534_345_454) }
  end

  # test_creating_too_large_romans test whether it is raised
  # InvalidIntegerValueError exception when the initializer of the
  # Roman class is called with an integer greater than 3000.
  def test_creating_too_large_romans
    assert_raises(InvalidIntegerValueError) { Roman.new(3_001) }
    assert_raises(InvalidIntegerValueError) { Roman.new(3_010) }
    assert_raises(InvalidIntegerValueError) { Roman.new(4_000) }
    assert_raises(InvalidIntegerValueError) { Roman.new(10_000) }
    assert_raises(InvalidIntegerValueError) { Roman.new(100_000_000_000_000) }
  end

  # test_creating_roman_with_invalid_data_type tests whether it is raised
  # InvalidDataTypeError exception when the initializer of the
  # Roman class is called with an object which is not an Integer or a String.
  def test_creating_roman_with_invalid_data_type
    assert_raises(InvalidDataTypeError) { Roman.new(:symbol) }
    assert_raises(InvalidDataTypeError) { Roman.new(4.5) }
    assert_raises(InvalidDataTypeError) { Roman.new(nil) }
    assert_raises(InvalidDataTypeError) { Roman.new(Roman.new(5)) }
    assert_raises(InvalidDataTypeError) { Roman.new(self) }
  end

  # test_correct_small_romans_with_operations test creating the Roman object
  # with a string representing a roman number and then it test basic
  # mathematical operations with the created roman numbers
  # (instances of Roman class).
  def test_correct_small_romans_with_operations
    roman1 = Roman.new('IX')
    roman2 = Roman.new('IV')
    roman3 = Roman.new('XLIX')
    roman4 = Roman.new('LXVI')
    assert_equal 36, roman1 * roman2
    assert_equal 115, roman3 + roman4
    assert_equal 151, roman1 * roman2 + roman3 + roman4
  end

  # combinations_of_small_romans creates and returns all roman numbers from 0
  # to 1000.
  # @return [Hash] The hash where the key is the integer (arabic number) and
  # the value is the string representing the roman number.
  def combinations_of_small_romans
    hundreds_order = { 0 => '', 100 => 'C', 200 => 'CC', 300 => 'CCC', 400 => 'CD', 500 => 'D', 600 => 'DC', 700 => 'DCC', 800 => 'DCCC', 900 => 'CM' }
    tens_order = { 0 => '', 10 => 'X', 20 => 'XX', 30 => 'XXX', 40 => 'XL', 50 => 'L', 60 => 'LX', 70 => 'LXX', 80 => 'LXXX', 90 => 'XC' }
    units_order = { 0 => '', 1 => 'I', 2 => 'II', 3 => 'III', 4 => 'IV', 5 => 'V', 6 => 'VI', 7 => 'VII', 8 => 'VIII', 9 => 'IX' }
    combinations = {}
    hundreds_order.each do |hundreds_integer, hundreds_roman|
      tens_order.each do |tens_integer, tens_roman|
        units_order.each do |units_integer, units_roman|
          roman_integer = hundreds_integer + tens_integer + units_integer
          roman_string = "#{hundreds_roman}#{tens_roman}#{units_roman}"
          combinations[roman_integer] = roman_string
        end
      end
    end
    combinations
  end

  # generate_all_combinations_from_1_to_3000 creates and returns
  # all roman numbers from 0 to 3000.
  # @return [Hash] The hash where the key is the integer (arabic number) and
  # the value is the string representing the roman number.
  def generate_all_combinations_from_1_to_3000
    all_combinations = { 3_000 => 'MMM' }
    (0..2).each do |count_m|
      ms_only = 'M' * count_m
      combinations_of_small_romans.each do |arabic_integer, roman_value|
        integer_value = count_m * 1_000 + arabic_integer
        roman_string_value = "#{ms_only}#{roman_value}"
        all_combinations[integer_value] = roman_string_value
      end
    end
    all_combinations
  end

  # test_correct_to_s_in_roman tests whether the method to_s works correctly
  # for all roman numbers from 1 to 3000.
  def test_correct_to_s_in_roman
    combinations = generate_all_combinations_from_1_to_3000
    combinations.each do |rome_integer, rome_string|
      next if rome_integer.zero?

      roman = Roman.new(rome_integer)
      assert_equal rome_string, roman.to_s
    end
  end

  # test_correct_to_rom_string tests whether the method to_rom of String class
  # works correctly for all strings representing a roman number from 1 to 3000.
  def test_correct_to_rom_string
    combinations = generate_all_combinations_from_1_to_3000
    combinations.each do |rome_integer, rome_string|
      next if rome_integer.zero?

      roman = rome_string.to_rom
      assert_equal rome_integer, roman.to_int
    end
  end

  # test_correct_to_rom_integer tests whether the method to_rom of Integer
  # class works correctly for all integers from 1 to 3000. It tests also the
  # functionality of to_s method for the returned instances of Roman class.
  def test_correct_to_rom_integer
    combinations = generate_all_combinations_from_1_to_3000
    combinations.each do |rome_integer, rome_string|
      next if rome_integer.zero?

      roman = rome_integer.to_rom
      assert_equal rome_string, roman.to_s
    end
  end
end
