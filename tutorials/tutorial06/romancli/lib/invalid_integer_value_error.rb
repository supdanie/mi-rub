# frozen_string_literal: true

# InvalidIntegerValueError is an error which is thrown when the initialize
# method of the Roman class is called outside the interval from 1 to 3000.
class InvalidIntegerValueError < StandardError
  # The message method returns the string describing the actual error
  # which is printed in the console. This string is printed in the
  # console after throwing it (when the constructor of the roman class
  # is called with a number outside the interval from 1 to 3000).
  #
  # @return [String] The string describing the actual error.
  def message
    "Error: Invalid integer value: > #{super}"
  end
end
