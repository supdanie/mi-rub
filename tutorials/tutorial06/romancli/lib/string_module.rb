# frozen_string_literal: true

# StringModule refines String to be convertable to a roman number.
module StringModule
  refine String do
    # Method to_rom converts the actual string to a roman number.
    #
    # @return [Roman] The roman number created from the actual string.
    def to_rom
      Roman.new(self)
    end
  end
end
