# frozen_string_literal: true

# InvalidStringValueError is an error which is thrown when the initializer
# of the Roman class is called with a String which is not representing
# a roman number from 1 to 3000.
class InvalidStringValueError < StandardError
  # This method returns a message which is thrown when the initializer
  # of the Roman class is called with a String which is not representing
  # a roman number from 1 to 3000. This string is also printed
  # in the console in this case.
  #
  # @return [String] The string representing a message which is thrown
  #   when the initializer of the Roman class is called with a String
  #   which is not representing a roman number from 1 to 3000.
  def message
    "Error: Invalid string value: > #{super}"
  end
end
