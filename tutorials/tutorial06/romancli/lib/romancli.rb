# frozen_string_literal: true

require 'thor'

require_relative './roman.rb'

# Romancli is a class for CLI for converting arabic numbers to roman numbers
# and a roman number to the arabic number of the same value.
class Romancli < Thor
  # to_roman method (or action) converts the given number to a string
  # represeting the roman number.
  # @param [String] num The given arabic number from the command line.
  desc 'to_roman NUM', 'It prints the roman number from the arabic number NUM.'
  def to_roman(num)
    if /[-]?\w*[0-9]+/.match(num).to_s != num || num == ''
      puts('You must enter a number.')
    elsif num.to_i <= 0
      puts('Number is too small.')
    elsif num.to_i > 3000
      puts('Number is too large.')
    else
      roman = Roman.new(num.to_i)
      puts(roman)
    end
  end

  # to_arabic method (or action) converts the given roman number to the arabic
  # number with the same value.
  # @param [String] roman The string with the roman number from the
  #   command line.
  desc 'to_arabic ROMAN', 'It prints the arabic number from the roman number ROMAN.'
  def to_arabic(roman)
    roman_num = Roman.new(roman)
    puts(roman_num.to_int)
  rescue InvalidStringValueError => e
    puts e.message
  end
end
