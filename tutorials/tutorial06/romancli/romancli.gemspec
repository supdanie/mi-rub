# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'romancli'
  s.version = '0.0.0'
  s.date = '2019-11-01'
  s.summary = 'CLI for converting arabic numbers to roman numbers and roman numbers to arabic numbers.'
  s.description = 'A simple romancli gem.'
  s.authors = ['Daniel Sup']
  s.email = 'supdanie@fit.cvut.cz'
  s.files = Dir['lib/*']
  s.executables = Dir['bin/*'].map { |f| File.basename(f) }
  s.homepage = 'https://rubygems.org/gems/romancli'
  s.license = 'MIT'
end
