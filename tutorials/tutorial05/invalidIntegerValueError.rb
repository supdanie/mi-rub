class InvalidIntegerValueError < StandardError
  def message
    "Error: Invalid integer value: > #{super}"
  end
end
