module IntegerModule
  refine Integer do 
    def eql? (other)
      if (other.class == Roman)
        self == other.value
      end
      self == other
    end
 

    def to_rom
      return Roman.new(self)
    end
  end
end
