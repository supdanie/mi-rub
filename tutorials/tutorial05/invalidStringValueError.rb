class InvalidStringValueError < StandardError
  def message
    "Error: Invalid string value: > #{super}"
  end
end
