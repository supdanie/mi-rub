require_relative "./integerModule.rb"
require_relative "./invalidDataTypeError.rb"
require_relative "./invalidStringValueError.rb"
require_relative "./invalidIntegerValueError.rb"

using IntegerModule

class Roman include Comparable, IntegerModule
  attr_accessor :value
  alias :to_int :value
  alias :to_i :value

  def initialize(value)
    if (value.class != Integer && value.class != String && value.class != Fixnum)
      raise InvalidDataTypeError, "Value must be an integer or a string."
    elsif value.class == String
      match = /([M]{0,2}((CM)|(CD)|([D]?[C]{0,3}))?((XC)|(XL)|([L]?[X]{0,3}))?((IX)|(IV)|([V]?[I]{0,3}))?)/.match(value) 
      if (match == nil || value == "")
        raise InvalidStringValueError, "The value must be a valid Roman number."
      elsif (match.to_s != value && value != "MMM")
        raise InvalidStringValueError, "The value must be a valid Roman number. The value must contain a Roman number from 1 to 3000, but the value must be a Roman number only."
      else
        
        self.value = self.string_to_value(value)
      end
    else
      if (value <= 0 || value > 3000)
        raise InvalidIntegerValueError, "Value must be positive (not zero or negative)."
      else
        self.value = value
      end
    end
  end

  def <=>(other)
    value <=> other
  end

  def eql?(other)
    other == value
  end

  def +(other)
    value + other
  end

  def -(other)
    value - other
  end

  def *(other)
    value * other
  end

  def /(other)
    value / other
  end

  def to_s
    value = self.value
    value_to_string = ""
    string_values_for_orders = [["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"], ["X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"], ["C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]]
    # procházíme polem - potřebuji index v poli string_values_for_orders a číslo 10, 100 nebo 1000 pro získání číslice v daném řádu hodnoty
    [0, 1, 2].each do |order|
      if (value % 10 != 0)
        order_index = order
        string_value_for_order = string_values_for_orders[order_index][(value % 10) - 1]
        value_to_string = "#{string_value_for_order}#{value_to_string}"
      end
      value = value / 10
    end
    
    (1..value).each do |x| 
       value_to_string = "M#{value_to_string}"
    end
    return value_to_string
  end

  def coerce(other)
    if (other.class != Roman)
      [other, self.value]
    else
      [self, other]
    end
  end

  
  def string_to_value (value)
    string_values_for_orders = [["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"], ["X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"], ["C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]]
      
      string_value = value
      integer_value = 0
      order = 1
      (0..2).each do |index|
        highest_found = 0
        best_value = ""
        (1..9).each do |number|
          rome_string = string_values_for_orders[index][number - 1]
          if string_value.include? rome_string 
            highest_found = number
            best_value = rome_string
            if number == 4
              break
            end
          end
        end
        string_value.gsub!(best_value, "")
        integer_value = integer_value + highest_found * order 
        order = order * 10
      end
      
      (0..value.length-1).each do |i|
        if value[i] == 'M'
          integer_value += 1000
        else
          break
        end
      end
      integer_value
  end

end

r = Roman.new(6)
s = Roman.new(7)
puts r + 5
puts 5 + r
puts r + s
puts 26 - s - r
puts 9 - r - s
puts s - 3
puts s - r
puts (1..100).first(s)
puts [r, s, r, s].sum
puts r / s
puts s / r
puts s * r
puts s.to_s

m = Roman.new(2449)
puts m.to_s
puts 5.eql? Roman.new(5)
puts Roman.new(5).eql? 5
puts 5 == Roman.new(5)
puts r == Roman.new(6)

puts 6.to_rom

xx = Roman.new("MMCMXCIX")
puts xx.to_int

xxa = Roman.new("XCI")
puts xxa.to_int

n = Roman.new(10)
puts n.to_s

m = Roman.new(100)
puts m.to_s

m = Roman.new(111)
puts m.to_s

