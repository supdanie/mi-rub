class InvalidDataTypeError < StandardError
  def message
    "Error: Invalid data type: > #{super}"
  end
end
