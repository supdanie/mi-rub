# Currency convertor - Web API

This appplication is a convertor from one currency to any other currency. You can give the application the source currency, amount of the source currency and the target currency. You must give the abbreviations which are used by the Czech national bank (ČNB).

This api contains only one source URL: `convert`. You must give parameters `amount`, `from` and `to`. The parameter `amount` is the amount of the source currency in the `from` parameter. The value of the `from` parameter is the abbreviation of the source currency (for example USD for american dollars). Of course the amount of the source currency must be a positive number. If some of the parameters are invalid, the aplication returns an error text with the status code 400.

 If you don't have an internet connection, the course list can't be loaded and it returns an error text with the status code 500.

 If you enter an invalid abbreviation of the source currency or the target currency, it returns an error text with the status code 400.

## How to run the API

Before the first run of the API you must have installed sinatra gem for defining web APIs. If you don't have installed sinatra gem, you can do it by:

`gem install sinatra`

You can run the API only by running the file convertor.rb as ruby source code. You can do it by command:

`ruby convertor.rb`

## How to call the API

You can call the API with three parameters in URL: `amount`, `from`, `to`.  The parameter `amount` is the amount of the source currency in the `from` parameter. The value of the `from` parameter is the abbreviation of the source currency (for example USD for american dollars). The value of the `to` parameter is the abbreviation of the target currency to which we want to convert the given amount of the source currency (for example EUR for euros).

Consider that the server runs at localhost (127.0.0.1) on port 4567. If you want for example to convert two euros to american dollars, you call the api with the url `http://127.0.0.1:4567/convert?amount=2&from=USD&to=EUR` by GET method. You can run the API from a web browser or by curl command:

`curl "http://127.0.0.1:4567/convert?amount=2&from=USD&to=EUR"`

The output of the application on 25.11.2019 will be:

`1.817`

It doesn't matter what order the parameters amount, from, to are in. Of course, you call call the API for the example mentioned higher by curl command for example by this way:

`curl "http://127.0.0.1:4567/convert?to=EUR&amount=2&from=USD"`