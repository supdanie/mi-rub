# frozen_string_literal: true

require 'sinatra'
require 'net/http'
require 'csv'

get '/convert' do
  amount = params['amount']
  from_currency = params['from']
  to_currency = params['to']
  check = check_input(amount, from_currency, to_currency)
  halt 400, check unless check.length.zero?

  courses = load_courses
  halt 500, courses unless courses.class == Hash
  check_and_count(courses, amount.to_f, from_currency, to_currency)
end

def check_input(amount, from, to)
  check_string = amount.nil? ? "#{check_string} Amount (parameter amount) must be given." : ''
  check_string = from.nil? ? "#{check_string} Source currency (parameter from) must be given." : check_string
  check_string = to.nil? ? "#{check_string} Target currency (parameter to) must be given." : check_string
  amount.to_f <= 0 ? "#{check_string} The amount must be positive." : check_string
end

def load_courses
  uri_string = 'https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt'
  uri = URI(uri_string)
  response = Net::HTTP.get_response(uri)
  txt_content = response.body.split("|kurz\n")[1]
  return 'The course list could not be found or loaded correctly.' if txt_content.nil?

  csv_content = txt_content.gsub(',', '.').gsub('|', ',')
  load_courses_from_string(csv_content)
rescue SocketError
  'There is an error with the internet connection.'
end

def load_courses_from_string(loaded_string)
  courses = { 'CZK' => [1, 1] }
  CSV.parse(loaded_string) do |line|
    currency = line[3]
    course = line[4].to_f
    amount = line[2].to_i
    courses[currency] = [amount, course]
  end
  courses
end

def check_currency_validity(courses, from, to)
  result = courses.key?(from) ? '' : 'The source currency (parameter from) is invalid.'
  courses.key?(to) ? result : "#{result} The target currency (parameter to) is invalid."
  halt 400, result unless result.length.zero?
end

def check_and_count(courses, amount, from, to)
  check_currency_validity(courses, from, to)

  from_currency = courses[from]
  to_currency = courses[to]
  new_amount = amount * (from_currency[1] / from_currency[0]) / (to_currency[1] / to_currency[0])
  new_amount.round(3).to_s
end
