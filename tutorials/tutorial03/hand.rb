class Hand

  attr_reader :length
  attr_reader :type

  def initialize (length, type)
    if (length.class == Integer || length.class == BigDecimal || length.class == BigDecimal)
      if (length < 0)
        raise("Length of the hand must be positive")
      end
      @length = length 
    else
      raise("Length of the hand must be a number") 
    end
    
    raise 'Invalid type of hand' unless [:poker, :slasher, :grabber].include? type
    @type = type
  end

  # The method inspect is used for printing the Hand in a nice format.
  def inspect
    return "{length: @length, type: @type}" 
  end
end
