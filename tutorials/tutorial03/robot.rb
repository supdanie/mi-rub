require_relative './speech.rb'
require_relative './hand.rb'

class Robot include Speech, Comparable

  attr_reader :name
  attr_reader :arms
  def initialize(name = "Name")
    @name = name
    @arms = []
  end

  def speak(text)
    p text
  end

  def introduce()
      number_of_arms = arms.count
      types = {}
      @arms.each do |arm|
        if types.key?(arm.type)
           types[arm.type] += 1
        else
           types[arm.type] = 1
        end
      end

      types_string = "I have these types of hands: "
      types.each { |key, value| types_string += "#{value} x #{key}, "}
      types_string.delete_suffix!(", ")
      speak("Hello, I am #{name}, I have #{number_of_arms} hands and #{types_string}")
  end
   
  def addArm(*arms)
    arms.each do |arm|
       if arm.class != Hand
         raise("You must add an object of Hand class, not a different object")
       end
    end
    arms.each do |arm|
       @arms.push(arm)
    end
  end


  def <=>(other)
    score() <=> other.score()
  end

  def score()
    score = 0
    score_for_arm = {:poker => 1, :slasher => 2, :grabber => 3}
    @arms.each do |arm|
      score += score_for_arm[arm.type]
    end
    score
  end

end

robot1 = Robot.new("Robot1")
robot1.addArm(Hand.new(30, :slasher), Hand.new(40, :poker), Hand.new(50, :grabber), Hand.new(60, :poker))
robot1.introduce()

robot2 = Robot.new("Robot2")
robot2.addArm(Hand.new(40, :grabber), Hand.new(55, :grabber), Hand.new(60, :slasher))
robot2.introduce()

robot1.annoy("My text", 15)

p robot1 < robot2
p robot1 == robot2
p robot1 > robot2

robot3 = Robot.new("Robot3")
robot3.addArm(Hand.new(45, :poker), Hand.new(21, :poker))
robot3.introduce()

p robot1.between?(robot3, robot2)
p robot3 < robot1

