module Speech
  
  def shout(text)
    speak(text.upcase)
  end
   
  def whisper(text)
    speak(text.downcase)
  end

  def annoy(text, n = rand(1..10))
    (1..n).each do |i|
      random_fact = rand(1..5)
      if (random_fact == 1)
        text.gsub!("a", " I am annoying now.")
        text << ". I am annoying now."
        speak(text)
      elsif (random_fact == 2)
        text << "annoying"
        speak(text.gsub!("annoying", "not annoying"))
      elsif (random_fact == 3)
        text << "!gnikaeps ylno am I .gniyonna ton ma I"
        speak(text.reverse!)
      elsif (random_fact == 4)
        text.gsub!("I am annoying", "I AM NOT ANNOYING. I AM ONLY SPEAKING. BLA BLA BLA.")
        speak(text)
      else
        text << ".gniyonna rof yrros ma I"
        text.reverse!
        speak(text)
      end
    end
  end
end
